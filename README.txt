
-- SUMMARY --

nodestates module is used to count & export number of
nodes of all the content types in a drupal installation by year.

-- USAGE --

After enabling this module site administrators are then able to
download a csv under content link admin/export-node-csv-by-year.
